#include "player.h"
#include <cmath>
#include <stdio.h>
 /* I made a comment */

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
 
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = true;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     this->side = side;
     
     if(side == BLACK)
     {
         opside = WHITE;
     }
     else
     {
         opside = BLACK;
     }   
     
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete(&b);
}

/*int Player::numPieces(Move *m, Dir d)
{
    int x = m->x;
    int y = m->y;
    
    int score = 0;
    int i = 1; // index
    
    // Add 1 for every opponent piece that is captured
    while((x + i * d.x > -1 && x + i * d.x < 8) && (y + i * d.y > -1 && y + i * d.y < 8)
        && (b.get(opside, x + i * d.x, y + i * d.y)))
    {
        score += 1;
        i += 1;
    }
    
    // If there wasn't a piece of own color in the direction, then score is 0
    i -= 1;
    if( x + i * d.x == 0 || x + i * d.x == 7 || y + i * d.y == 0 || y + i * d.y == 7)
    {
        return 0;
    }
    else
    {
        return score + 1; // add 1 for own piece at the end
    }
}*/

/* 
 * Returns the number of pieces that are changed to Player's side after
 * the move.
 */ 
int Player::numFrontiers(Side s, Board *input)
{
    int frontiers = 0;
    Side op;
    Board *cpy = input->copy();
    
    if(s == WHITE)
    {
        op = BLACK;
    }
    else
    {
        op = WHITE;
    }

    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            if(cpy->get(s, i, j))
            {
                for(int k = -1; k < 2; k++)
                {
                    if(i == 0 && k == -1)
                    {
                        continue;
                    }
                    else if(i == 7 && k == 1)
                    {
                        continue;
                    }
                    
                    for(int l = -1; l < 2; l++)
                    {
                        if(j == 0 && l == -1)
                        {
                            continue;
                        }
                        else if(j == 7 && l == 1)
                        {
                            continue;
                        }
                        else
                        {
                            if(!cpy->get(s, i + k, j + l) && !cpy->get(op, i + k, j + l))
                            {
                                frontiers++;
                                Move move(i + k, j + l);
                                cpy->doMove(&move, op);
                            }
                        }
                    }
                }
            }
        }
    }

    delete(cpy);
    return frontiers;

}

/*
 * Returns the mobility of a side.
 */
 int Player::mobility(Side s, Board *input)
 {
     int mobile = 0;
     for(int i = 0; i < 8; i++)
     {
         for(int j = 0; j < 8; j++)
         {
              Move move(i, j);
              if(input->checkMove(&move, s))
              {
                  mobile++;
              }
          }
      }
      return mobile;
  }

/* Returns a score for a position with the following scoring:
 * Corner               +10
 * Edge                 +3
 * Next to corner       -4
 * Diag next to corner  -8
 * Other                 0
 */ 
int Player::positionScore(Move *move)
{
    if(move == NULL)
    {
		return -5;
	}
	
    int x = move->x;
    int y = move->y;
    
    if(x == 0 || x == 7)
    {
        // Corner
        if(y == 0 || y == 7)
        {
            return 10;
        }
        // Next to corner
        if(y == 1 || y == 6)
        {
            return -4;
        }
        // Edge
        else
        {
            return 6;
        }
    }
    else if(y == 0 || y == 7)
    {
        // Next to corner
        if(x == 1 || x == 6)
        {
            return -4;
        }
        // Edge
        else
        {
            return 6;
        }
    }
    // Diag next to corner
    else if((y == 1 || y == 6) && (x == 1 || x == 6))
    {
        return -8;
    }
    // One of the middle pieces ("Other")
    else
    {
        return 0;
    }
}

int Player::pieceScore(Side side, Board *temp)
{
    Side op;
    
    if(side == BLACK)
    {
        op = WHITE;
    }
    else
    {
        op = BLACK;
    }
    
    int piece_score = temp->count(side) - temp->count(op);

    return piece_score;
}
    
int Player::alpha_beta(Move *move, int depth, int alpha, int beta, bool maximize, Board *input)
{
   //fprintf(stderr, "depth: %d\n", depth);
   if (depth == 0) // or node is terminal node (?)
   {
	   int a = 0;
	   Board *bd = input->copy();
	   if(maximize)
	   {
		   bd->doMove(move, opside);
		   a = pieceScore(this->side, bd) - positionScore(move) - numFrontiers(this->side, bd);
	   }
	   else
	   {
		   bd->doMove(move, side);
		   a = pieceScore(this->side, bd) + positionScore(move)- numFrontiers(this->side, bd);
	   }

       delete(bd);
       return a;
   }
   if(input->isDone())
   {
       return 0;
   }
   if (maximize)
   {
       Board *bd = input->copy();
       bd->doMove(move, opside);
       if (bd->hasMoves(side))
       {
           for (int i = 0; i < 8; i++)
           {
               for (int j = 0; j < 8; j++)
               {
                   // For each "child"...
                   Move m (i, j);
                   if (bd->checkMove(&m, side))
                   {
                       alpha = max(alpha, alpha_beta(&m, depth - 1, alpha, beta, false, bd));
                       if (beta <= alpha)
                       {
                           return alpha;
                       }
                   }
               }
           }
       }
       else
       {
           alpha = max(alpha, alpha_beta(NULL, depth - 1, alpha, beta, false, bd));
       }
       return alpha;
       delete(bd);
   }
   else
   {
       Board *bd = input->copy();
       bd->doMove(move, side);
       if(bd->hasMoves(opside))
       {
           for (int i = 0; i < 8; i++)
           {
               for (int j = 0; j < 8; j++)
               {
                   Move m(i, j);
                   if (bd->checkMove(&m, opside))
                   {
                       beta = min(beta, alpha_beta(&m, depth - 1, alpha, beta, true, bd));
                       if (beta <= alpha)
                       {
                           return beta;
                       }
                   }
               }
           }
       }
       else
       {
           beta = min(beta, alpha_beta(NULL, depth - 1, alpha, beta, true, bd));
       }
       return beta;
       delete(bd);
   }
}
/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     
    /* Update board state with opponent's move */
    b.doMove(opponentsMove, opside);
    
    /* Return a move */

    //If we can make a move...
    if(b.hasMoves(this->side))
    {
        int max = -1000;
        Move *move = new Move(0, 0);
        
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                Move m(i, j);
                
                if(b.checkMove(&m, this->side))
                {

                    int score = alpha_beta(&m, 5, -1000, 1000, false, &b);

                    if(score > max)
                    {
                        max = score;
                        move->setX(i);
                        move->setY(j);
                    }
                }
            }
        }

        b.doMove(move, side);
        return move;
    }
                        
    return NULL;
}
