Contributions

Jingwen
-Wrote first version of alpha beta
-Wrote first version of minimax
-Fixed Christine's oopsies

Christine
-Wrote basic heuristic
-Wrote frontiers
-Fixed Jingwen's oopsies

Both (most of the time was spent here/we coded together)
-Debugged/fixed minimax and alpha beta
-Oopsies fixing

Improvements
-Used frontiers to minimize number of places the opponent could attack
-Used position score to maximize the number of corners and edges
-Used alpha-beta/minimax to search the decision tree more quickly
	-We searched to a depth of 5 and beat the given test players quickly
-Tried to use mobility but seemed to make our player worse
