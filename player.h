#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <time.h>
#include "common.h"
#include "board.h"
using namespace std;

class Player {
	
private:
	Side side;
	Side opside;
	Board b;

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int alpha_beta(Move *move, int depth, int alpha, int beta, bool maximize, Board *input);
    int pieceScore(Side side, Board *temp);
    int positionScore(Move *move);
    int mobility(Side s, Board *input);
	int numFrontiers(Side s, Board *input);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
