#ifndef __COMMON_H__
#define __COMMON_H__

#include <string.h>

enum Side { 
    WHITE, BLACK
};

class Move {
   
public:
    int x, y;
    Move(int x, int y) {
        this->x = x;
        this->y = y;        
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
};

class Dir {
	
public:
	int x, y;
	Dir(int s){
		if(s == 'N')
		{
			this->x = -1;
			this->y = 0;
		}
		else if(s == 'S')
		{
			this->x = 1;
			this->y = 0;
		}
		else if(s == 'W')
		{
			this->x = 0;
			this->y = -1;
		}
		else if(s == 'E') 
		{
			this->x = 0;
			this->y = 1;
		}
		else if(s == 'A') // Northwest
		{
			this->x = -1;
			this->y = -1;
		}
		else if(s =='B') // Southwest
		{
			this->x = 1;
			this->y = -1;
		}
		else if(s == 'C') // Northeast
		{
			this->x = -1;
			this->y = 1;
		}
		else if(s == 'D') // Southeast
		{
			this->x = 1;
			this->y = 1;
		}
	}
	
	~Dir() {}
};

#endif
